<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\yearController;
use App\Http\Controllers\ClassController;
use App\Http\Controllers\ScoreController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\AttendantController;
use App\Http\Controllers\ScoreListController;
use App\Http\Controllers\attentListController;
use App\Http\Controllers\StudentListController;
use App\Http\Controllers\RegisterStudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::resource('admin/class',ClassController::class);
Route::resource('admin/year',yearController::class);
Route::resource('admin/student',StudentController::class);
Route::resource('admin/attendant',AttendantController::class);
Route::resource('admin/score',ScoreController::class);
Route::resource('admin/register',RegisterStudentController::class);
Route::resource('admin/home',HomeController::class);

Route::get('admin/class/{id}',[ClassController::class,'destroy']);
Route::get('admin/delete/{id}',[yearController::class,'destroy']);
Route::get('admin/edit/{id}',[yearController::class,'edit']);

Route::resource('admin/list',StudentListController::class);

Route::resource('admin/score_list',ScoreListController::class);
Route::resource('admin/attend_list',attentListController::class);

Route::get('admin/add',function(){
    return view('admin.RegisterStudent.add');
});
Route::get('/findclass/{id}',[RegisterStudentController::class, 'findclass'])->name('getclass');

