<?php

namespace App\Models;

use App\Models\Classes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class year extends Model
{
    use HasFactory;
    protected $table="years";
    protected $fillable=[
        'year'
    ];

    public function classes(){
        return $this->hasMany(Classes::class);
    }
}
