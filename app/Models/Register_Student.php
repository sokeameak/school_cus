<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Register_Student extends Model
{
    use HasFactory;
    protected $table="register_students";
    protected $fillable=([
        'year_id','class_id','student','sex','dob','status'
    ]);
}
