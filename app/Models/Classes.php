<?php

namespace App\Models;

use App\Models\year;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Classes extends Model
{
    use HasFactory;
    protected $table="classes";
    protected $fillable=([
        'year_id','class','name'
    ]);
    public function year(){
        return $this->belongTo(year::class);
    }
}
