<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class register extends Model
{
    use HasFactory;
    protected $table="registers";
    protected $fillable=([
        'year_id','class_id','student','sex','dob','status'
    ]);
}
