<?php

namespace App\Models;

use App\Models\Family;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class student extends Model
{
    use HasFactory;
    protected $table="students"
    protected $fillable=[
        'firstname','lastname','sex','dob','address','family_id'
    ];
    public function families(){
        return $this->belongsTo(Family::class);
    }
}
