<?php

namespace App\Models;

use App\Models\student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Family extends Model
{
    use HasFactory;
    protected $table="families";
    protected $fillable=[
        'father','mother','job','address','phone'
    ];
    public function students(){
        return $this->hasMany(student::class);
    }
}
