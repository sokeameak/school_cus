@extends('admin.layouts')
@include('admin.includes.link')

@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body style="font-family:Khmer OS">
<div class="head">
                <div class="title">ចូរបំពេញព័ត៏មានរបស់សិស្ស</div>
            </div><br>
            <div style="font-family:khmer os;box-shadow:6px 6px 20px -4px rgba(0,0,0,0.70);">
                <label for="year"style="font-family:khmer os;font-size:20px;">ឆ្នាំសិក្សា</label>
              
               <select  id="year" name="year_id">
                   <option selected="false" style="color:red;">ជ្រើសរើសឆ្នាំសិក្សា</option>
                
                </select>
               
              
              
                <label for="year"style="font-family:khmer os">ថ្នាក់ទី</label>
                    <select name="class" id="class">
                        <option selected="false">ជ្រើសរើសថ្នាក់</option>

                    </select>

                <label for="year"style="font-family:khmer os">ឈ្មោះថ្នាក់</label>
                <input type="text" placeholder="ឈ្មោះថ្នាក់"/>
   
  
            </div><br>
            <form action="">
                    <table class="table">
                        <tr>
                            <td>ឈ្មោះសិស្ស</td>
                            <td><input type="text" class="form-control" name="student"placeholder="វាយបញ្ចូលឈ្មោះសិស្ស"></td>
                            <td>ភេទ</td>
                            <td>
                                <select name="sex" id="sex" class="form-control">
                                    <option selected disabled>ជ្រើសរើសភេទ</option>
                                    <option value="ប្រុស">ប្រុស</option>
                                    <option value="ស្រី">ស្រី</option>
                                </select>
                            </td>
                            <td>ថ្ងៃខែឆ្នាំកំណើត</td>
                            <td><input type="date" class="form-control" name="dob"></td>
                        </tr>
                        <tr>
                            <td>ទូរស័ព្ទ</td>
                            <td><input type="text" class="form-control" name="student"placeholder="វាយបញ្ចូលឈ្មោះសិស្ស"></td>
                            <td>ស្ថានភាពចុះឈ្មោះ</td>
                            <td>
                                <select name="status" id="">
                                <option value="" selected disabled>ជ្រើសរើស</option>
                                    <option value="new">សិស្សថ្មី</option>
                                    <option value="fall">សិស្សត្រួតថ្នាក់</option>
                                    <option value="in">សិស្សផ្ទេរចូល</option>
                                    <option value="again">ចូលរៀនសាឡើងវិញ</option>
                                </select>
                            
                            </td>
  
                               <td>អ្នកចុះឈ្មោះ</td>
                               <td><input type="text" class="form-control" name="student"placeholder="អ្នកចុះឈ្មោះ"></td>
                            
                        </tr>
                       <tr>
                           <td>បញ្ហាសុខភាព</td>
                           <td>
                         <select>
                         <option value="អត់">អត់</option>
                             <option value="ខ្វះអាហារូបត្ថម្ភ">ខ្វះអាហារូបត្ថម្ភធ្ងន់ធ្ងរ</option>
                             <option value="ជម្ងឺប្រចាំកាយ">សុខភាព/ជម្ងឺប្រចាំកាយ</option>
                         </select>
                        </td>
                      
                        
                       </tr>
                        <tr>
                            <td></td>
                            <td><a href="{{url('admin/student')}}" class="btn btn-danger" >ត្រឡប់</a></td>
                            <td></td>
                            <td><a href="" class="btn btn-success">ចុះឈ្មោះបន្ដទៀត</a></td>
                            <td></td>
                            <td><a href="" class="btn btn-info">ចុះឈ្មោះ</a></td>
                        </tr>
                    </table>
                 </form>
                 <table>
                    <tr>
                        
                    </tr>
                 </table>
                 
</body>
</html>
@endsection