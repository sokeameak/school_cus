@include('admin.includes.link')
@extends('admin.layouts')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>welcome</title>
    <link rel="stylesheet" href="{{asset('css/home/index.css')}}">

    <script src="{{asset('js/jquery/jquery_menu.js')}}"></script>
   <link rel="stylesheet" href="{{asset('css/style_pop-up-form.css')}}">

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}
<!-- this below style form add -->
  <script defer src="{{asset('js/add.js')}}">
// <script src="jquery-3.5.1.min.js"></script>
</head>
<body>
        <div style="font-family:Khmer OS; float:right;">
                 <button type="button" class="btn btn-primary" id="show">
                 ដាក់សិស្សក្នុងថ្នាក់
                </button>
        </div>
    <div style="font-family:khmer os;display:flex;box-shadow:6px 6px 29px -4px rgba(0,0,0,0.75);padding-left:10px;">

           <select name="year" id="sel_year" class="year"​ >
           <option selected="selected">ជ្រើសរើសឆ្នាំសិក្សា</option>
           @foreach($year as $year)
                <option value="{{$year->id}}">{{$year->year}}</option>
           @endforeach
           </select>

            <select name="class" id="class_id" class="class">
                <option selected="false">
                    ជ្រើសរើសថ្នាក់
                </option>

            </select>

            <input type="text"class="form-control" placeholder="ឈ្មោះថ្នាក់"/>
            <input type="button" class="btn btn-info" value="ស្វែងរក">
    </div>
<br>
       <table class="table" style="font-family:Khmer OS;font-size:20px;">
            <thead >
                <th style="width:4px;"><input type="checkbox" name="chbox"/></th>
                <th style="width:4px;">

                    <div class="sign"><a href="{{url('admin/register/create')}}" class="button"><i class="fas fa-plus-circle"></i></a></div></th>
                <th>ឈ្មោះសិស្ស</th>
                <th>ភេទ</th>
                <th>ថ្ងៃខែឆ្នាំកំណើត</th>
                <th>ថ្នាក់ទី</th>
                <th>ស្ថានភាព</th>
                <th>លេខទូរស័ព្ទ</th>
            </thead>


       </table>

       @include('admin.RegisterStudent.add')


       <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="year"]').on('change', function() {
                var year_id = $(this).val();
                if(year_id) {
                    $.ajax({
                        url: '/findclass/'+year_id,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            $('select[name="class"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="class"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });


                        }
                    });
                }else{
                    $('select[name="class"]').empty();
                }
            });
        });
    </script>

</body>
</html>
@endsection



