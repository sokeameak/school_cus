<nav class="mt-2" style="font-family: Khmer OS">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <a href="/admin/home" class="nav-link">
            <i class="fas fa-home"></i>
              <p>
              ទំព័រដើម
               
              </p>
            </a>
          <li class="nav-item has-treeview menu-open">
           
          <li class="nav-item has-treeview menu-open">
        <a href="#" class="nav-link">
            <i class="fas fa-school"></i>
          <p>
           សាលារៀន
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">

        <li class="nav-item">
            <a href="/admin/register" class="nav-link">
                <i class="far fa-address-card"></i>
              <p>​ចុះឈ្មោះចូលរៀន</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{url('/admin/class')}}" class="nav-link">
                <i class="far fa-address-card"></i>
              <p>ថ្នាក់រៀន</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('/admin/student')}}" class="nav-link">
                <i class="fas fa-user-graduate"></i>
              <p>សិស្ស</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('/admin/score')}}" class="nav-link">
                <i class="fas fa-sort-numeric-up-alt"></i>
              <p>គ្រប់គ្រងពិន្ទុ</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('/admin/attendant')}}" class="nav-link">
                <i class="fas fa-broadcast-tower"></i>
              <p>គ្រប់គ្រងអវត្ដមាន</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('/admin/year')}}" class="nav-link">
                <i class="fas fa-book-medical"></i>
              <p>ឆ្នាំសិក្សា</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('/admin/year')}}" class="nav-link">
                <i class="fas fa-book-medical"></i>
              <p>អ្នកប្រើប្រាស់ប្រព័ន្ធ</p>
            </a>
          </li>
        </ul>
      </li>
             
          
        </ul>
      </nav>