@extends('admin.layouts')
@include('admin.includes.link')
@section('content')


  
<button class="btn btn-danger"><a href="{{url('/admin/year/create')}}" style="color:black;font-family:Khmer OS">បង្កើតឆ្នាំសិក្សា</button>

<div class="year" style="padding:20px;box-shadow: box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">

<table class="table" style="font-size:24px;border:2px solid black;">
    <tr style="background:	#696969; color:white">
        <th>កូដ</th>
        <th>ឆ្នាំសិក្សា</th>
        <th>ថ្ងៃទីបង្កើត</th>
        <th>បញ្ជា</th>
    </tr>
    
    @foreach($years as $year)
    <tr>
        <td>{{$year['id']}}</td>
        <td>{{$year['year']}}</td>
        <td>{{$year['created_at']}}</td>
        <td>
        <a href = 'edit/{{ $year->id }}'><i class="far fa-edit"></i></a>
        <a href = 'delete/{{ $year->id }}'><i class="fas fa-minus-circle"></i></a>   
      
        </td>
    </tr>
    @endforeach
  
</table>
    
</div>

@endsection

@push('js')
      

@endpush
