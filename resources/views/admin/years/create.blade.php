@extends('admin.layouts')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Year </title>
    @include('admin.includes.link')
    <link rel="stylesheet" href="{{ asset('css/create_year.css')}}">
</head>
<body>
  @if(Session::get('success'))
    <div class="alert alert-success">
      {{Session::get('success')}}
    </div>
  @endif
  @if(Session::get('fail'))
    <div class="alert alert-danger">
      {{Session::get('fail')}}
    </div>
  @endif
    <form action="{{route('year.store')}}" class="form" method="POST" >
    @csrf
    <div class="form-group" style="font-family: Khmer OS">
      <label for="email">ឆ្នាំសិក្សា</label>
      <input type="text" class="form-control" id="year" placeholder="2020-2021" name="year">
    </div>
    <div class="form-group">
    
   <input type="submit" Value="OK">
    </form>
</body>
</html>
@endsection