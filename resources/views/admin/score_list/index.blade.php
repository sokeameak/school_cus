@include('admin.includes.link')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
      table {
  table-layout:fixed;
  width: inherit !important;
  font-size:15px;
  font-family:"Khmer OS";
}
th {
  width:80px !important;
  height:30px !important;
  max-width:80px !important;
  max-height:30px !important;
  min-width:80px !important;
  min-height:30px !important;
  text-align:center;
}
    </style>
</head>
<body>
   
    <table class="table table-bordered">
        <tr style="background-color:grey;color:white;">
            <th>អត្ដលេខ</th>
            <td style=" width:180px !important;
  height:50px !important;
  max-width:180px !important;
  max-height:50px !important;
  min-width:180px !important;
  min-height:50px !important;">ឈ្មោះសិស្ស</td>
            <th>ភេទ</th>
            <th>ស្ដាប់</th>
            <th>សរសេរ</th>
            <th>អាន</th>
            <th>និយាយ</th>
            <th>ចំនួន</th>
            <th>រង្វាស់រង្វាល់</th>
            <th>ធរណីមាត្រ</th>
            <th>ពិជគណិត</th>
            <th>ស្តិតិ</th>
            <th>នព្វន្ធ</th>
            <th>វិទ្យាសាស្រ្ដ</th>
            <th>សីលធម៌-ពលរដ្ឋ</th>
            <th>កីឡា</th>
            <th>សុខភាព-អនាម័យ</th>
            <th>បំណិនជីវិត</th>
            <th>មធ្យមភាគ</th>
        
            
            
        </tr>
        <tr>
            <td>0003</td>
            <td>មាគ គា</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0004</td>
            <td>ឈឿម លីស្សា</td>
            <td>ស្រី</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0004</td>
            <td>បេង គឹមសួរ</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0005</td>
            <td>ថុក ផល្លី</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0006</td>
            <td>ហាន សុផារ៉ាត</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0007</td>
            <td>សេង គ្រី</td>
            <td>ស្រី</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0004</td>
            <td>បេង គឹមសួរ</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0005</td>
            <td>ថុក ផល្លី</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0003</td>
            <td>មាគ គា</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0004</td>
            <td>ឈឿម លីស្សា</td>
            <td>ស្រី</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0004</td>
            <td>បេង គឹមសួរ</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
        <tr>
            <td>0005</td>
            <td>ថុក ផល្លី</td>
            <td>ប្រុស</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
            <td>7</td>
            <td>9</td>
            <td>6</td>
            <td>8</td>
            <td>5</td>
        </tr>
    </table>

</body>
</html>
