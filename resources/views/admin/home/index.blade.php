@extends('admin.layouts')
@include('admin.includes.link')

@section('content')
@include('admin.home.includes.css')
   <div class="main">
            <div class="row">
                <div class="col-sm-4">
                    <div class="card">
                        <div class="title">
                            ចំនួនគ្រូក្នុងឆ្នាំ
                        </div>
                        <div class="body">
                        @include('admin.home.teacher')
                        </div>
                        <div class="foot">
                        
                        </div>
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="title">
                            ចំនួនសិស្សក្នុងឆ្នាំ
                        </div>
                        <div class="body">
                        @include('admin.home.student')
                        </div>
                        <div class="foot">
                        
                        </div>
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="title">
                            ចំនួនថ្នាក់រៀនក្នុងឆ្នាំ
                        </div>
                        <div class="body">
                        @include('admin.home.room')
                        </div>
                        <div class="foot">
                                <a href="" class="btn btn-info">more</a>
                        </div>
                        
                    </div>
                </div>
            </div>
       
    <div class="chat" style="margin-left:10px">
    
    </div>
   
   </div>
@endsection