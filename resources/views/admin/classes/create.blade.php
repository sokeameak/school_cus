@extends('admin.layouts')
@include('admin.includes.link')
@section('content')
<div class="container">
 
<h3 style="font-family:Khmer OS Muol Light;font-size:24px; padding:10px;">បង្កើតថ្នាក់រៀន</h3>
 
<form class="form-inline"​ action="{{route('class.store')}}" method="POST" style="font-family:Khmer OS;font-size:24px;">
@csrf
<label class="sr-only" for="UsernameInlineFormInput">ឆ្នាំសិក្សា</label>
<label for="grade"> ឆ្នាំសិក្សា</label>
<div class="form-group">
    
        <select class="form-control" id="sel1" name="year_id">
            @foreach ($item as $year)
                <option value="{{$year['id']}}">{{$year['year']}}</option>
            @endforeach
        </select>
</div>
  <label class="sr-only" for="UsernameInlineFormInput">Username</label>
 
  <div class="input-group mb-2 mr-sm-2">
    <label for="grade">| ថ្នាក់ទី</label>
    <input type="text" class="form-control" name="class"id="grade" placeholder="ថ្នាក់ទី....">
 
  </div>
  <div class="input-group mb-2 mr-sm-2">
    <label for="grade">| ឈ្មោះ</label>
    <input type="text" class="form-control" name="class_name"id="grade" placeholder="ឈ្មោះថ្នាក់">
 
  </div>
 
  <button type="submit" class="btn btn-info mb-2">បង្កើត</button>
 
</form>
 
</div>

@endsection