@extends('admin.layouts')
@include('admin.includes.link')
@section('content')

<div class="year" style="padding:20px;box-shadow: box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
<div style="font-family:khmer os;padding-left:10px; border-radius: 10px;box-shadow:6px 6px 29px -4px rgba(0,0,0,0.75);">
    <label for="year"style="font-family:khmer os;font-size:20px;">ឆ្នាំសិក្សា</label>
    <input type="text" placeholder="ជ្រើសរើសឆ្នាំសិក្សា"/>
    
    <input type="button" value="ស្វែងរក">

    <button type="button" class="btn btn-danger" style="float:right;"><a href="{{url('admin/class/create')}}" style="color:white">បង្កើតថ្នាក់</a></button>
</div><br>
<table class="table" style="font-size:20px;font-family:Khmer OS;">
    <thead >
        <th style="color:#25383C">កូដ</th>
        
        <th><a href="{{url('admin/class/create')}}" style="color:blue;"><i class="fas fa-plus-square"></i></a>ថ្នាក់ទី</th>
        <th>ឈ្មោះថ្នាក់</th>
        <th>ឈ្មោះគ្រូកាន់ថ្នាក់</th>
    </thead>
    
    @foreach($item as $class)
    <tr>
        <td>{{$class['id']}}</td>
        
        <td>{{$class['class']}}</td>
        
        <td>{{$class['name']}}</td>
        <td>
        <button type="button" class="btn btn-primary"><i class="fas fa-eye"></i></button>
       <button type="button" class="btn btn-success"><i class="fas fa-edit"></i></button>
       <a href = 'class/{{ $class->id }}'><i class="fas fa-minus-circle"></i></a> 
        </td>
    </tr>
    @endforeach
  
</table>
    
</div>

@endsection

