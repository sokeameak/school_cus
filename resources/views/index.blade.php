@include('admin.includes.link')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
   <link rel="stylesheet" href="{{asset('css/index/index.css')}}">
   <script src="{{asset('js/index.js')}}"></script>
</head>
<body>
    <div class="main">
        <div class="banner">
               <div class="menubar">
                    <div class="navbar">
                        <ul>
                           <li><a href="#"><i class="fas fa-home"></i>ទំព័រដើម</a></li>
                           <li><a href="#"><i class="far fa-address-book"></i>ទំនាក់ទំនង</a></li> 
                           <li><a href="#"><i class="fas fa-address-card"></i>អំពីសាលា</a></li> 
                           <li id="login"><a href="#"><i class="fas fa-sign-in-alt"></i>ប្រើប្រាស់</a></li>  
                            
                        </ul>
                    </div>
               </div>
            <div class="text">
            ការអប់រំប្រកបដោយគុណភាព សមធម៌ និងបរិយាបន្ន សម្រាប់ទាំងអស់គ្
            </div>
        </div><br>
        <div class="content">
                    <div class="row">
                        <div class="col-sm-4">
                           <div class="card">
                           <i class="fas fa-address-card"></i>
                                ទទួលចុះឈ្មោះកុមារ
                            ចាប់ពីអាយុ ៣ ឆ្នាំកន្លះឡើងទៅ
                           </div>
                       
                          
                        </div>
                        <div class="col-sm-4">
                        <div class="card">
                        <i class="fas fa-graduation-cap"></i>

                               ការអប់រំចំណេះដឹងទូទៅ នៅសាលាបឋមសិក្សាស្វាយធំ
                           </div>
                        </div>
                        <div class="col-sm-4">
                        <div class="card">
                           
                           <i class="fas fa-phone-volume"></i>
                           Tel: 096 86 89 680 
                           </div>
                        </div>
                    </div>





        </div>
        <div class="popup" id="popup">
            <div class="popup-content" style="font-family:khmer os">
                <img src="{{asset('images/del.png')}}" alt="close" class="close" id="close">
                <form action="{{route('home.store')}}" method="post" class="form">
                @csrf
                    <div class="form-group">
                        <label for="user">អ្នកប្រើប្រាស់</label>
                        <input type="text" name="user" class="form-control" placeholder="ឈ្មោះអ្នប្រើប្រាស់">
                    </div>
                    <div class="form-group">
                        <label for="password">លេខសម្ងាត់</label>
                        <input type="password" name="password" class="form-control" placeholder="វាយលេខសម្ងាត់">
                    </div>
                    <input type="submit" name="submit" class="btn btn-info" value="LOGIN" style="float:right">
                </form>
            </div>
        </div>
        <div class="foot">

        </div>
    </div>
</body>
</html>